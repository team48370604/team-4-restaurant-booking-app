package com.example.restaurantbookingsystem

import android.content.Context
import android.util.Log
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material.icons.rounded.Favorite
import androidx.compose.material.icons.rounded.Info
import androidx.compose.material.icons.rounded.Place
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.example.restaurantbookingsystem.ui.theme.clrBlack
import com.example.restaurantbookingsystem.ui.theme.clrBlackLight
import com.example.restaurantbookingsystem.ui.theme.clrLightGrey
import com.example.restaurantbookingsystem.ui.theme.clrOrange



@Composable
fun TextFieldWithIcon(
    hnt: String,
    lbl:String,
    kydOptions: KeyboardOptions,
    vslTrans: VisualTransformation,
    imgVector: ImageVector,
    onValChng:(text:String) -> Unit,
    supportText: String = "",
    isError: Boolean = false
    ) {

    var text by remember { mutableStateOf("") }

    OutlinedTextField(
        placeholder = {Text(text = lbl)},
        value = text,
        onValueChange = {
            text = it
            Log.d(lbl, text)
            onValChng(it)

                        },
        label = { Text(lbl) },
        keyboardOptions = kydOptions,
        visualTransformation = vslTrans,
        colors = OutlinedTextFieldDefaults.
        colors(
            focusedTextColor = clrBlack,
            unfocusedTextColor = clrBlack,
            focusedContainerColor = clrLightGrey,
            unfocusedContainerColor = clrLightGrey,
            disabledContainerColor = clrLightGrey,
            cursorColor = clrOrange,
            focusedBorderColor = Color.Black,
            unfocusedBorderColor = Color.Black,
            focusedLeadingIconColor = clrBlack,
            unfocusedLeadingIconColor = clrBlack,
            focusedLabelColor = clrBlack,
            unfocusedLabelColor = clrBlack,
            focusedPlaceholderColor = clrBlack,
            unfocusedPlaceholderColor = clrBlackLight,
        ),
        leadingIcon = {
            Icon(
                imageVector = imgVector,
                contentDescription = null,
                modifier = Modifier.padding(0.dp),
            )
        },
        supportingText = {
            Text(text = supportText)
        },
        isError = isError

    )
}

@Composable
fun ElvButton(enabled: Boolean = true , onClick: () -> Unit, btnTxt: String, modifier: Modifier) {
    ElevatedButton(
        enabled = enabled,
        modifier = modifier,
        onClick = {

            onClick() },
//        shape = MaterialTheme.shapes.extraSmall,
        shape = RectangleShape,
        colors = ButtonDefaults.elevatedButtonColors(containerColor = clrOrange),
        ) {
        Text(btnTxt, fontSize = 18.sp , style =  MaterialTheme.typography.bodyLarge, color = Color.White, modifier = Modifier
            .align(alignment = Alignment.CenterVertically))
    }
}

@Composable
fun TxtBtn(onClick: () -> Unit, txt:String) {
    TextButton(
        onClick = { onClick() }
    ) {
        Text(txt, textAlign = TextAlign.Center, color = clrBlack, style = MaterialTheme.typography.bodySmall)
    }
}

@Composable
fun ListBtnBox(location: String, onClk: () -> Unit) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
//            .background(Color.Blue)
            .clickable { onClk() }
    ) {
        Column {
            Row {
                Icon(imageVector = Icons.Default.LocationOn, contentDescription = "")
                Spacer(
                    modifier = Modifier
                        .padding(horizontal = 10.dp)
                )
                Text(text = location, fontSize = 20.sp)
            }
            Spacer(modifier = Modifier.padding(vertical = 10.dp))
        }
    }
}

@Composable
fun TitleText(
    txt: String,
    fontsize: Int,
    txtStyle: androidx.compose.ui.text.TextStyle = MaterialTheme.typography.headlineLarge,
    clr: Color = clrOrange,
    txtAlign: TextAlign = TextAlign.Center,
    modifier:Modifier = Modifier
    ) {
    Text(
        text = txt,
        style = txtStyle,
        color = clr,
        fontSize = fontsize.sp,
        softWrap = true,
        lineHeight = 50.sp,
        textAlign = txtAlign,
        modifier = modifier
    )
}


@Composable
fun ElvCard(restaurantID:Int, restaurantName: String, imgURL:String, cuisineType:String, onClk: () -> Unit) {
    ElevatedCard(
        elevation = CardDefaults.cardElevation(
            defaultElevation = 10.dp
        ),
        colors = CardDefaults.elevatedCardColors(
            containerColor = clrLightGrey,
            contentColor = clrBlack
        ),
        modifier = Modifier
            .requiredHeight(height = 300.dp)
            .fillMaxWidth()
            .fillMaxSize()
            .padding(vertical = 10.dp)
            .clickable { onClk() }
    ) {
        AsyncImage(
            model = ImageRequest.Builder(LocalContext.current)
                .data(imgURL)
                .crossfade(true)
                .build(),
            contentDescription = null,
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
        )
        Spacer(modifier = Modifier.height(10.dp))
        Row(modifier = Modifier
            .fillMaxWidth(1f),
                horizontalArrangement = Arrangement.SpaceBetween,) {
            Text(
                text = restaurantName,
                modifier = Modifier
                    .padding(top = 0.dp, start = 15.dp,)
                    .weight(.5f),
                textAlign = TextAlign.Left,
                fontFamily = FontFamily.Cursive,
                fontSize = 35.sp,
                fontWeight = FontWeight.Bold,
            )

        }
        Row(modifier = Modifier
            .padding(top = 10.dp, start = 10.dp, bottom = 5.dp)
            .fillMaxWidth(1f)) {
            Icon(
                imageVector = Icons.Rounded.Favorite,
                modifier = Modifier
                    .size(20.dp)
                , contentDescription = "Favorite icon")
            Text(
                text = cuisineType,
                modifier = Modifier
                    .padding(horizontal = 10.dp)
                    .weight(.5f),
                textAlign = TextAlign.Left,
            )
        }
        Row(modifier = Modifier
            .padding(top = 5.dp, start = 10.dp, bottom = 15.dp)
            .fillMaxWidth(1f)) {
            Icon(
                imageVector = Icons.Rounded.Info,
                modifier = Modifier
                    .size(20.dp)
                , contentDescription = "Place icon")
            Text(
                text = "Restaurant ID: $restaurantID",
                modifier = Modifier
                    .padding(horizontal = 10.dp)
                    .weight(.5f),
                textAlign = TextAlign.Left,
            )
        }
    }
}

@Composable
fun IndeterminateCircularIndicator(isLoading: Boolean) {
    var loading by remember { mutableStateOf(isLoading) }


    if (!loading) return

    CircularProgressIndicator(
        modifier = Modifier.width(64.dp),
        color = MaterialTheme.colorScheme.secondary,
        trackColor = MaterialTheme.colorScheme.surfaceVariant,
    )
}
fun containsOnlyAlphabetsAndSpaces(str: String): Boolean {
    return str.all { it.isLetter() || it.isWhitespace() }
}

fun areAllFieldsFilled(name: String, email:String, password:String, phoneNum:String): Boolean {
    return (!(name.isEmpty() || email.isEmpty() || password.isEmpty() || phoneNum.isEmpty()))

}


