package com.example.restaurantbookingsystem.screens

import android.content.Context
import android.graphics.Paint.Align
import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.graphics.toColorInt
import androidx.navigation.NavHostController
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.example.restaurantbookingsystem.ElvButton
import com.example.restaurantbookingsystem.GlobalVars
import com.example.restaurantbookingsystem.IndeterminateCircularIndicator
import com.example.restaurantbookingsystem.MySingleton
import com.example.restaurantbookingsystem.ui.theme.clrBlack
import com.example.restaurantbookingsystem.ui.theme.clrLightGrey
import com.example.restaurantbookingsystem.ui.theme.clrOrange
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONException
import org.json.JSONObject
import java.nio.charset.Charset
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

fun convertStringToList(str: String): MutableList<String> {
    if(str == "[]") {   return mutableListOf() }

    str.substring(1, str.length - 1)
    val str1 = str.replace("\\s+".toRegex(), "") // Remove whitespace
    var c = 0
    val stringToList = mutableListOf<String>()

    for (i in str1.indices) {

            if (str1[i] == ',') {
                c++
            } else {
                if (stringToList.size <= c) {
                    stringToList.add(str1[i].toString())
                } else {
                    stringToList[c] = stringToList[c] + str1[i] // Append character to existing string
                }
            }
        }
    Log.d("Response log", "Printing the list $stringToList ::convertStringToList")
    return stringToList
}
@Composable
fun SlotsDetail(navController: NavHostController, context: Context, slotList: String) {
    var startTime = remember {   mutableStateOf("")   }


    val slotList1 = convertStringToList(slotList)
    Log.i("Response log","SlotList1 size is ${slotList1.size} ::SlotsDetail")
    Log.i("Response log","SlotList1 is $slotList1 ::SlotsDetail")
    val selectedClr = remember { mutableStateOf(clrBlack)  }
    val selectedSlot = remember {mutableStateOf("")}
    Column(
        modifier = Modifier
            .fillMaxSize(1f)
            .fillMaxWidth(1f)


    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth(1f)
                .height(100.dp)
                .background(color = Color.White)
                .shadow(elevation = 0.5.dp, shape = RoundedCornerShape(0.5.dp)),
            contentAlignment = Alignment.Center

        ) {
            Text(
                text =  "Selected Slot: ${selectedSlot.value}",
                modifier = Modifier
                    .padding(start = 15.dp, end = 10.dp)
                    .fillMaxWidth(1f),
                fontSize = 25.sp,
                fontWeight = FontWeight.Bold
            )

        }
        LazyColumn(
            Modifier
                .padding(horizontal = 15.dp)
                .fillMaxWidth(1f),
            state = rememberLazyListState(),
        ){
            items(slotList1) { item ->
                Box(
                    modifier = Modifier
                        .padding(horizontal = 16.dp, vertical = 10.dp)
                        .fillMaxWidth()
                        .background(color = Color.White)
                        .shadow(elevation = 0.5.dp, shape = RoundedCornerShape(0.5.dp))
                ) {
                    Row {
                        Icon(
                            modifier = Modifier
                                .padding(horizontal = 5.dp)
                                .align(alignment = Alignment.CenterVertically),
                            imageVector = Icons.Filled.ArrowForward, contentDescription = "Slot List icon" )
                        Text(
                            text = item,
                            modifier = Modifier
                                .clickable {
                                    startTime.value = item.substring(0..4)
                                    Log.d("Response log", "Start time is $startTime ::slotDetails")
                                    selectedClr.value = clrOrange
                                    GlobalVars.bookingTime = item
                                    selectedSlot.value = item
                                    Log.i("Response log", "Selected text is $item ::slotDetails")
                                }
                                .padding(horizontal = 20.dp, vertical = 15.dp),

                            color = when (GlobalVars.bookingTime) { // Use GlobalVars for central state
                                item -> selectedClr.value  // Highlight clicked item
                                else -> Color(clrBlack.value)// Maintain regular color for others
                            },


                            )
                    }
                }
            }
        }

        Spacer(modifier = Modifier.weight(1f))
        ElvButton(
            onClick = {
                Log.d("Response log", "Value of start time is ${startTime.value}::slotDetails")
                CoroutineScope(Dispatchers.Main).launch {
                    if(startTime.value != "") {
                        val bookingID = getBookingID(context, GlobalVars.restId, GlobalVars.bookingDate, GlobalVars.noOfGuests, startTime.value )
                        navController.navigate("bookingDonePage/$bookingID")
                }
                    else if(startTime.value == "" ) {
                        Toast.makeText(context, "Start time is empty", Toast.LENGTH_SHORT).show()
                        Log.e("Response log", "Start time is empty ::SlotsDetail")
                    }


                }

                 },
            btnTxt ="GET BOOKING DETAILS" ,
            modifier = Modifier
                .fillMaxWidth(1f)
                .requiredHeight(100.dp)
                .align(Alignment.CenterHorizontally)
        )
    }
}

suspend fun getBookingID(context: Context, restaurantId: Int, formattedDate: String, nguests: Int, startTime: String):Int {
    Log.d("Response log" , " Parameter received in getBookingID is $restaurantId, $formattedDate, $nguests , $startTime ::getBookingID")
    return suspendCoroutine { continuation ->
        val url = "${GlobalVars.url}/api/savingSlot/"
        val token = GlobalVars.token
        try {

            val myStringRequest: StringRequest = object : StringRequest(
                Method.POST,
                url,
                Response.Listener<String?> { response ->
                    val temp = response
                    val jsonObject = JSONObject(temp)

                    if (jsonObject.has("message")) {

                        val msg = jsonObject.getString("message")
                        Log.i("Response log", "Message is $msg ::getBookingID")
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
                    }
                    else if (jsonObject.has("booking_id")) {
                            val bID = jsonObject.getInt("booking_id")
                        Log.i("Response log", "Message is $bID ::getBookingID")

                            continuation.resume(bID)
                    }
                    else {
                        Log.d("Response log", "In the else block. Condition doesn't match for both if and else if::getBookingID")
                    }
                },
                Response.ErrorListener { error ->
                    Log.e("Response log", "Error is $error ::getBookingID")
                    continuation.resumeWithException(error)
                }
            ) {
                override fun getHeaders(): MutableMap<String, String> {
                    val params: MutableMap<String, String> = HashMap()
                    params.put("Authorization", "bearer $token")
                    params.put("content-type", "application/Json; charset=UTF-8")
                    return params
                }

                override fun getBody(): ByteArray {
                    val params: LinkedHashMap<Any, Any> = LinkedHashMap()
                    params["restaurant_id"] = restaurantId
                    params["selected_date"] = formattedDate
                    params["nguests"] = nguests
                    params["start_time"] = startTime
                    val paramString = JSONObject(params as Map<*, *>?).toString()
                    return paramString.toByteArray(Charset.defaultCharset())
                }
            }
            MySingleton.getInstance(context).addToRequestQueue(myStringRequest)
        } catch (e: JSONException) {
            Log.e("Response log", "Error is $e. Response log is in the catch block::getBookingID")
            continuation.resumeWithException(e)
        }
    }


}