package com.example.restaurantbookingsystem.screens

data class RestaurantDetail(
    val id: Int,
    val name: String,
    val image: String,
    val location: String,
    val cuisineType: String
)
