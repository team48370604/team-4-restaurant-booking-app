package com.example.restaurantbookingsystem.screens

import android.content.Context
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material.icons.rounded.Face
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.example.restaurantbookingsystem.ElvButton
import com.example.restaurantbookingsystem.GlobalVars
import com.example.restaurantbookingsystem.MySingleton
import com.example.restaurantbookingsystem.TxtBtn
import com.example.restaurantbookingsystem.areAllFieldsFilled
import com.example.restaurantbookingsystem.containsOnlyAlphabetsAndSpaces
import com.example.restaurantbookingsystem.ui.theme.clrBlack
import com.example.restaurantbookingsystem.ui.theme.clrOrange
import org.json.JSONException
import org.json.JSONObject
import java.nio.charset.Charset

@Composable
fun SignUpPage( context: Context, navController: NavHostController) {

    val url = "${GlobalVars.url}/api/register"

    val isPasswordVisible = remember { mutableStateOf(false) }
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,

    ) {
        Text(
            text = "Restaurant Booking System",
            style = MaterialTheme.typography.headlineLarge,
            color = clrOrange,
            modifier = Modifier.padding(vertical = 60.dp),
        )

        val enteredName = remember {mutableStateOf("")}
        val isErrorInName = remember {
            derivedStateOf {
                if(enteredName.value.isEmpty()) {
                    false
                }
                else{
                    !containsOnlyAlphabetsAndSpaces(enteredName.value)
                }
            }
        }
        NameField(enteredName, isErrorInName)




        val enteredEmail = remember {mutableStateOf("")}
        val isErrorInEmail = remember {
            derivedStateOf {
                if(enteredEmail.value.isEmpty()) {
                false
                }
                else {
                    Patterns.EMAIL_ADDRESS.matcher(enteredEmail.value).matches().not()

                }
            }
        }
        EmailField(enteredEmail, isErrorInEmail)



        val enteredPassword = remember {mutableStateOf("")}
        val isErrorInPassword = remember {
            derivedStateOf {
                if(enteredPassword.value.isEmpty()) {
                    false
                }
                else {
                    enteredPassword.value.length < 8
                }
            }
        }
        PasswordField(enteredPassword, isPasswordVisible, isErrorInPassword)




        val enteredPhone = remember {mutableStateOf("")}
        val isErrorInPhone = remember {
            derivedStateOf {
                if(enteredPhone.value.isEmpty()) {
                    false
                }
                else {
                    Patterns.PHONE.matcher(enteredPhone.value).matches() && enteredPhone.value.length == 10
                }
            }
        }

        PhoneField(enteredPhone,isErrorInName)

        Spacer(modifier = Modifier.padding(vertical = 15.dp))

        val btnEnabled = remember {
            derivedStateOf {
                mutableStateOf(
                    areAllFieldsFilled(
                    enteredName.value,
                    enteredEmail.value,
                    enteredPassword.value,
                    enteredPhone.value)
                            &&
                            (isErrorInName.value || isErrorInEmail.value || isErrorInPassword.value || isErrorInPhone.value)

                )
            }
        }

        ElvButton(
            enabled = btnEnabled.value.value,
            onClick =
            signUpOnClick(
                url,
                context,
                navController,
                enteredName,
                enteredEmail,
                enteredPassword,
                enteredPhone
            )
            , "Sign Up",
            Modifier
                .width(150.dp)
                .height(60.dp))

        Spacer(modifier = Modifier.padding(vertical = 10.dp))

        TxtBtn(onClick = { /*TODO*/
            navController.navigate("LoginPage")
                         }, txt = "Already have an account?\n Sign In" )
    }
}

@Composable
private fun signUpOnClick(
    url: String,
    context: Context,
    navController: NavHostController,
    enteredName: MutableState<String>,
    enteredEmail: MutableState<String>,
    enteredPassword: MutableState<String>,
    enteredPhone: MutableState<String>
): () -> Unit = {

    val myStringRequest: StringRequest = object : StringRequest(
        Method.POST, url,
        Response.Listener<String?> { response ->
            Log.i("Response log Success", "SignUp response is - $response")
            try {
                val jsonObject = JSONObject(response)

                if (response != null) {
                    val jsonVal = jsonObject.getString("message")
                    Log.i("Response log jsonVal", jsonVal)
                    JSONObject(response)
                    Log.i("Response log Sign up", "$response")

                    when (jsonVal) {
                        "Register successful" -> {
                            Toast.makeText(
                                context,
                                "Registration successful $response",
                                Toast.LENGTH_SHORT
                            ).show()
                            navController.navigate("loginPage")
                        }
                        "User already exists!!!" -> {
                            Toast.makeText(
                                context,
                                "User already exist. Please login. $response",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        else -> {
                            Log.i("Response log signup", response)
                            Toast.makeText(
                                context,
                                "Failed to Signup $response",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                } else {
                    Log.i("Response log signup", "$response")
                    Toast.makeText(context, "Failed to Signup $response", Toast.LENGTH_SHORT)
                        .show()
                }

            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }, Response.ErrorListener { error ->
            Log.e("Response log failed", "Login failed. Failed error is  - $error")

            Toast.makeText(context, "Error! Failed to post data.", Toast.LENGTH_SHORT).show()
        }) {

        override fun getHeaders(): MutableMap<String, String> {
            val params: MutableMap<String, String> = HashMap()
            params.put("content-type", "application/Json; charset=UTF-8")
            return params
        }

        override fun getBody(): ByteArray {
            val params: LinkedHashMap<Any, Any> = LinkedHashMap()
            Log.d(
                "Response log",
                "${enteredName.value}, ${enteredEmail.value}, ${enteredPassword.value}, ${enteredPhone.value} ::signup"
            )
            params["name"] = enteredName.value
            params["email"] = enteredEmail.value
            params["password"] = enteredPassword.value
            params["contact_number"] = enteredPhone.value

            val paramString = JSONObject(params as Map<*, *>?).toString()

            Log.d("TAG", paramString)

            return paramString.toByteArray(Charset.defaultCharset())
        }
    }
    MySingleton.getInstance(context).addToRequestQueue(myStringRequest)


}

@Composable
private fun PhoneField(
    enteredPhone: MutableState<String>,
    isErrorInPhone: State<Boolean>
) {
    OutlinedTextField(
        value = enteredPhone.value,
        onValueChange = { it: String ->
            enteredPhone.value = it
        },
        placeholder = { Text("Enter your Mobile number") },
        label = { Text("Mobile number") },
        singleLine = true,
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Phone),
        leadingIcon = { Icon(Icons.Default.Phone, contentDescription = "Phone Icon") },
        supportingText = {
            if (isErrorInPhone.value) {
                Text(text = "Enter a valid Mobile no.", fontSize = 14.sp)
            }
        },
        isError = isErrorInPhone.value
    )
}

@Composable
private fun PasswordField(
    enteredPassword: MutableState<String>,
    isPasswordVisible: MutableState<Boolean>,
    isErrorInPassword: State<Boolean>
) {
    OutlinedTextField(
        value = enteredPassword.value,
        onValueChange = { it: String ->
            enteredPassword.value = it
        },
        placeholder = { Text("Enter your Password") },
        label = { Text("Password") },
        singleLine = true,
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
        visualTransformation =
        if (isPasswordVisible.value) {
            VisualTransformation.None
        } else {
            PasswordVisualTransformation()
        },
        leadingIcon = { Icon(Icons.Default.Lock, contentDescription = "Lock Icon") },
        trailingIcon = {
            Icon(
                imageVector = Icons.Rounded.Face,
                tint = clrBlack,
                contentDescription = "Password Visible Icon",
                modifier = Modifier.clickable {
                    isPasswordVisible.value = !isPasswordVisible.value
                }
            )
        },
        supportingText = {
            if (isErrorInPassword.value) {
                Text(text = "Password length must be greater than 8 ", fontSize = 14.sp)
            }
        },
        isError = isErrorInPassword.value
    )
}

@Composable
private fun EmailField(
    enteredEmail: MutableState<String>,
    isErrorInEmail: State<Boolean>
) {
    OutlinedTextField(
        value = enteredEmail.value,
        onValueChange = { it: String ->
            enteredEmail.value = it
        },
        placeholder = { Text("Enter your Email") },
        label = { Text("Email") },
        singleLine = true,
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Email),
        leadingIcon = { Icon(Icons.Default.Email, contentDescription = "Email Icon") },
        supportingText = {
            if (isErrorInEmail.value) {
                Text(text = "Enter a valid email address")
            }
        },
        isError = isErrorInEmail.value
    )
}

@Composable
private fun NameField(
    enteredName: MutableState<String>,
    isErrorInName: State<Boolean>
) {
    OutlinedTextField(
        value = enteredName.value,
        onValueChange = { it: String ->
            enteredName.value = it
        },
        placeholder = { Text("Enter your Name") },
        singleLine = true,
        label = { Text("Full name") },
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text),
        leadingIcon = { Icon(Icons.Default.Person, contentDescription = "Person Icon") },
        supportingText = {
            if (isErrorInName.value) {
                Text(text = "Enter a valid name")
            }
        },
        isError = isErrorInName.value
    )
}

