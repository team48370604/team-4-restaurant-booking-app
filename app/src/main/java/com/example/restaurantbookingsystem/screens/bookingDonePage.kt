package com.example.restaurantbookingsystem.screens

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.example.restaurantbookingsystem.ElvButton
import com.example.restaurantbookingsystem.GlobalVars
import com.example.restaurantbookingsystem.MySingleton
import com.example.restaurantbookingsystem.TitleText
import com.example.restaurantbookingsystem.ui.theme.clrBlack
import com.example.restaurantbookingsystem.ui.theme.clrBlackLight
import org.json.JSONException
import org.json.JSONObject
import java.nio.charset.Charset
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

@Composable
fun BookingDonePage(navHostController: NavHostController, context: Context, restName: String, bookingDate: String, noOfGuest: Int, bookingID: Int) {
//    var guestDetails = remember { mutableStateOf<MutableList<String>>(mutableListOf()) }
    val guestDetails:MutableList<String> = remember { mutableListOf() }
    LaunchedEffect(Unit) {
        guestDetails.addAll(getGuestDetails(context, bookingID))
    }
    Column(modifier = Modifier
        .fillMaxSize(0.9f)
        .padding(start = 15.dp)

        ) {
        Spacer(modifier = Modifier
            .fillMaxWidth(1f)
            .padding(vertical = 10.dp))
        TitleText(txt = "Enjoy your Meal🍴", fontsize = 30)
        TitleText(txt = "Booking id: $bookingID", fontsize = 15, clr = clrBlackLight, txtStyle = MaterialTheme.typography.bodyMedium)
        Spacer(modifier = Modifier.padding(vertical = 20.dp))
        TitleText(
            txt = restName,
            fontsize = 20,
            txtStyle = MaterialTheme.typography.bodyMedium,
            clr = clrBlack,
            txtAlign = TextAlign.Left
        )
        TitleText(
            txt = "Hotel Park Plaza, Gurgaon",
            fontsize = 15,
            txtStyle = MaterialTheme.typography.bodyMedium,
            clr = clrBlackLight,
            txtAlign = TextAlign.Left
        )

        Spacer(modifier = Modifier.padding(vertical = 20.dp))
        TitleText(
            txt = "Booking Date & Time",
            fontsize = 20,
            txtStyle = MaterialTheme.typography.bodyMedium,
            clr = clrBlack,
            txtAlign = TextAlign.Left
        )
        TitleText(
            txt = "$bookingDate (${GlobalVars.bookingTime})",
            fontsize = 15,
            txtStyle = MaterialTheme.typography.bodyMedium,
            clr = clrBlackLight,
            txtAlign = TextAlign.Left
        )

        Spacer(modifier = Modifier.padding(vertical = 20.dp))
        TitleText(
            txt = "No. of Guest",
            fontsize = 20,
            txtStyle = MaterialTheme.typography.bodyMedium,
            clr = clrBlack,
            txtAlign = TextAlign.Left
        )
        TitleText(
            txt = noOfGuest.toString(),
            fontsize = 15,
            txtStyle = MaterialTheme.typography.bodyMedium,
            clr = clrBlackLight,
            txtAlign = TextAlign.Left
        )

        Spacer(modifier = Modifier.padding(vertical = 20.dp))
        TitleText(
            txt = "Guest Details",
            fontsize = 20,
            txtStyle = MaterialTheme.typography.bodyMedium,
            clr = clrBlack,
            txtAlign = TextAlign.Left
        )
        if(guestDetails.size > 0) {
            TitleText(
                txt = guestDetails[0],
                fontsize = 15,
                txtStyle = MaterialTheme.typography.bodyMedium,
                clr = clrBlackLight,
                txtAlign = TextAlign.Left
            )
            TitleText(
                txt = guestDetails[1],
                fontsize = 15,
                txtStyle = MaterialTheme.typography.bodyMedium,
                clr = clrBlackLight,
                txtAlign = TextAlign.Left
            )
        }

//        Spacer(modifier = Modifier.weight(1f))  // Spacer to push content to the top

}

    Column(modifier = Modifier
        .fillMaxWidth(1f)) {
        Spacer(modifier = Modifier.weight(1f))
        ElvButton(
            enabled = true,
            onClick = {
                navHostController.navigate("LocationSelect")
            },
            btnTxt = "Book Another Restaurant",
            Modifier
                .requiredHeight(80.dp)
                .fillMaxWidth(1f)
        )
    }

}

suspend fun getGuestDetails(context: Context, bookingID: Int):MutableList<String> {
    Log.d("Response log" , " Parameter received in getGuestDetails is $bookingID::getGuestDetails")
    return suspendCoroutine { continuation ->
        val url = "${GlobalVars.url}/api/finalDetails/"
        val token = GlobalVars.token
        val guestDetails :MutableList<String> = mutableListOf()
        try {
            val myStringRequest: StringRequest = object : StringRequest(
                Method.POST,
                url,
                Response.Listener<String?> { response ->
                    val jsonObject = JSONObject(response)

                    if (jsonObject.has("customer_name")) {
                        val customerName = jsonObject.getString("customer_name")
                        val contactNumber = jsonObject.getString("contact_number")
//                        guestDetails.addAll(listOf(customerName, contactNumber))
                        guestDetails.add(customerName)
                        guestDetails.add(contactNumber)
                        Log.i("Response log", "Customer name is $customerName and contact number is $contactNumber ::getGuestDetails")
                        Toast.makeText(context, "Booking Confirmed", Toast.LENGTH_SHORT).show()
                        continuation.resume(guestDetails)
                    }
                    else if (jsonObject.has("message")) {
                        val msg = jsonObject.getString("message")
                        Log.i("Response log", "Message is $msg ::getGuestDetails")


                    }
                    else {
                        Log.d("Response log", "In the else block. Condition doesn't match for both if and else if::getGuestDetails")
                    }
                },
                Response.ErrorListener { error ->
                    Log.e("Response log", "Error is $error ::getGuestDetails")
                    continuation.resumeWithException(error)
                }
            ) {
                override fun getHeaders(): MutableMap<String, String> {
                    val params: MutableMap<String, String> = HashMap()
                    params.put("Authorization", "bearer $token")
                    params.put("content-type", "application/Json; charset=UTF-8")
                    return params
                }

                override fun getBody(): ByteArray {
                    val params: LinkedHashMap<Any, Any> = LinkedHashMap()
                    params["booking_id"] = bookingID
                    val paramString = JSONObject(params as Map<*, *>?).toString()
                    return paramString.toByteArray(Charset.defaultCharset())
                }
            }
            MySingleton.getInstance(context).addToRequestQueue(myStringRequest)
        } catch (e: JSONException) {
            Log.e("Response log", "Error is $e. Response log is in the catch block::getGuestDetails")
            continuation.resumeWithException(e)
        }
    }
}


@Preview(showBackground = true, showSystemUi = true)
@Composable
fun BookingDonePreview() {
    BookingDonePage(
        navHostController = rememberNavController(),
        context = LocalContext.current,
        restName = "ABC",
        bookingDate = "01-01-2004",
        noOfGuest = 5,
        bookingID = 1,
    )
}
