package com.example.restaurantbookingsystem.screens

import android.content.Context
import android.util.Log
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.rounded.LocationOn
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.example.restaurantbookingsystem.ElvCard
import com.example.restaurantbookingsystem.GlobalVars
import com.example.restaurantbookingsystem.IndeterminateCircularIndicator
import com.example.restaurantbookingsystem.MySingleton
import com.example.restaurantbookingsystem.TextFieldWithIcon
import com.example.restaurantbookingsystem.TitleText
import com.example.restaurantbookingsystem.ui.theme.clrBlack
import com.example.restaurantbookingsystem.ui.theme.clrBlackLight
import com.example.restaurantbookingsystem.ui.theme.clrLightGrey
import com.example.restaurantbookingsystem.ui.theme.clrOrange
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.net.URLEncoder
import java.nio.charset.Charset
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


suspend fun getCardDetails(context: Context, searchCity: String): MutableList<RestaurantDetail> {
    return suspendCoroutine { continuation ->
        val url = "${GlobalVars.url}/api/restaurants"
        val token = GlobalVars.token
        val myList: MutableList<RestaurantDetail> = mutableListOf()
        Log.i("Response log", "Token is $token ::getCardDetails")

        try {
            val myStringRequest: StringRequest = object : StringRequest(
                Method.POST,
                url,
                Response.Listener<String?> { response ->
                    val temp = response
                    val jsonObject = JSONObject(temp)
                    if (jsonObject.has("results")) {
                        val restaurantsArray = jsonObject.getJSONArray("results")
                        for (i in 0 until restaurantsArray.length()) {
                            val resultObject = restaurantsArray.getJSONObject(i)
                            val id = resultObject.getInt("id")
                            val name = resultObject.getString("name")
                            val image = resultObject.getString("image")
                            val cuisineType = resultObject.getString("cuisine_type")
                            val location = resultObject.getString("location")

                            val restObj = RestaurantDetail(id, name, image, location, cuisineType)
                            Log.d("Response log", "In the for loop. Location is $location ::getLocationList")
                            myList.add(restObj)
                        }
                        Log.i("Response log", "Restaurant List is $myList")
                    }
                    continuation.resume(myList)
                },
                Response.ErrorListener { error ->
                    Log.e("Response log", "Error is $error ::getCardDetails")
                    continuation.resumeWithException(error)
                }
            ) {
                override fun getHeaders(): MutableMap<String, String> {
                    val params: MutableMap<String, String> = HashMap()
                    params.put("Authorization", "bearer $token")
                    params.put("content-type", "application/Json; charset=UTF-8")
                    return params
                }

                override fun getBody(): ByteArray {
                    val params: LinkedHashMap<Any, Any> = LinkedHashMap()
                    params["location"] = searchCity
                    val paramString = JSONObject(params as Map<*, *>?).toString()
                    return paramString.toByteArray(Charset.defaultCharset())
                }
            }
            MySingleton.getInstance(context).addToRequestQueue(myStringRequest)
        } catch (e: JSONException) {
            Log.e("Response log", "Error is $e. Response log is in the catch block::getCardDetails")
            continuation.resumeWithException(e)
        }
    }
}



@Composable
fun RestaurantsList(navController: NavHostController, context: Context, selectedCity: String) {

    var myList  = remember { mutableStateOf(mutableListOf<RestaurantDetail>()) }
    LaunchedEffect(Unit) {
        myList.value =  getCardDetails(context, selectedCity)
    }
    Log.d("Response log", "Printing the size of the list ${myList.value.size}")

    var searchText by remember { mutableStateOf(TextFieldValue()) }
    val filteredList: List<RestaurantDetail> = remember(searchText) {
        myList.value.filter {
            it.name.contains(searchText.text, ignoreCase = true)
        }
    }


    Column(
        modifier = Modifier
            .fillMaxHeight()
            .padding(horizontal = 20.dp)
            .fillMaxSize(1f)
    ) {
        Box(modifier = Modifier.padding(top = 10.dp)) {
            TitleText(txt = "Restaurants in your Area😍", fontsize = 40)
        }
        OutlinedTextField(
            modifier = Modifier
                .fillMaxWidth(1f)
                .padding(vertical = 10.dp),
            placeholder = { Text(text = "Search a Restaurant") },
            value = searchText,
            onValueChange = {
                searchText = it
            },
            label = { Text("Location") },
            singleLine = true,
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Search,
                keyboardType = KeyboardType.Text
            ),
            visualTransformation = VisualTransformation.None,
            colors = OutlinedTextFieldDefaults.colors(
                focusedTextColor = clrBlack,
                unfocusedTextColor = clrBlack,
                focusedContainerColor = clrLightGrey,
                unfocusedContainerColor = clrLightGrey,
                disabledContainerColor = clrLightGrey,
                cursorColor = clrOrange,
                focusedBorderColor = Color.Black,
                unfocusedBorderColor = Color.Black,
                focusedLeadingIconColor = clrBlack,
                unfocusedLeadingIconColor = clrBlack,
                focusedLabelColor = clrBlack,
                unfocusedLabelColor = clrBlack,
                focusedPlaceholderColor = clrBlack,
                unfocusedPlaceholderColor = clrBlackLight,
            ),
            leadingIcon = {
                Icon(
                    imageVector = Icons.Filled.Menu,
                    contentDescription = null,
                    modifier = Modifier.padding(0.dp),
                )
            },
        )

        if(myList.value.isEmpty()) {
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                IndeterminateCircularIndicator(isLoading = true)
            }
        }
        else {
            IndeterminateCircularIndicator(isLoading = false)
            LazyColumn {
                items(if(filteredList.isEmpty() || searchText.text == "") myList.value
                else filteredList) { item ->
                    ElvCard(
                        item.id,
                        item.name,
                        item.image,
                        item.cuisineType,

                        onClk = {
                            searchText = TextFieldValue("")
                            val encodedImageUrl = URLEncoder.encode(item.image, "UTF-8")
                            Log.d("Response log", "Encoded URL is $encodedImageUrl ::restaurantList")
                            if(item.image.isNotEmpty()) {   navController.navigate("restaurantBook/${item.id}/${item.name}/$encodedImageUrl")  }

                        }
                    )

                }
            }
        }

    }

}