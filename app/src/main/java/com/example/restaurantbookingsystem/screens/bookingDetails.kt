package com.example.restaurantbookingsystem.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.restaurantbookingsystem.ElvButton
import com.example.restaurantbookingsystem.TitleText
import com.example.restaurantbookingsystem.ui.theme.clrBlack
import com.example.restaurantbookingsystem.ui.theme.clrBlackLight
import okhttp3.internal.addHeaderLenient

@Composable
fun BookingDetails(navHostController: NavHostController, restaurantName: String, restaurantAddress: String, bookingDate: String, formattedTime:String,  nguest:String, phoneNum: String) {
    Column(
        modifier = Modifier
            .fillMaxSize()
    ) {
        Spacer(modifier = Modifier.padding(vertical = 20.dp))
        TitleText(
            txt = restaurantName,
            fontsize = 20,
            txtStyle = MaterialTheme.typography.bodyMedium,
            clr = clrBlack,
            txtAlign = TextAlign.Left
        )
        TitleText(
            txt = restaurantAddress,
            fontsize = 15,
            txtStyle = MaterialTheme.typography.bodyMedium,
            clr = clrBlackLight,
            txtAlign = TextAlign.Left
        )

        Spacer(modifier = Modifier.padding(vertical = 20.dp))
        TitleText(
            txt = "Booking Date",
            fontsize = 20,
            txtStyle = MaterialTheme.typography.bodyMedium,
            clr = clrBlack,
            txtAlign = TextAlign.Left
        )
        TitleText(
            txt = bookingDate,
            fontsize = 15,
            txtStyle = MaterialTheme.typography.bodyMedium,
            clr = clrBlackLight,
            txtAlign = TextAlign.Left
        )
        TitleText(
            txt = formattedTime,
            fontsize = 15,
            txtStyle = MaterialTheme.typography.bodyMedium,
            clr = clrBlackLight,
            txtAlign = TextAlign.Left
        )

        Spacer(modifier = Modifier.padding(vertical = 20.dp))
        TitleText(
            txt = "No. of Guest",
            fontsize = 20,
            txtStyle = MaterialTheme.typography.bodyMedium,
            clr = clrBlack,
            txtAlign = TextAlign.Left
        )
        TitleText(
            txt = nguest,
            fontsize = 15,
            txtStyle = MaterialTheme.typography.bodyMedium,
            clr = clrBlackLight,
            txtAlign = TextAlign.Left
        )

        Spacer(modifier = Modifier.padding(vertical = 20.dp))
        TitleText(
            txt = "Guest Details",
            fontsize = 20,
            txtStyle = MaterialTheme.typography.bodyMedium,
            clr = clrBlack,
            txtAlign = TextAlign.Left
        )

        TitleText(
            txt = phoneNum,
            fontsize = 15,
            txtStyle = MaterialTheme.typography.bodyMedium,
            clr = clrBlackLight,
            txtAlign = TextAlign.Left
        )
        Spacer(modifier = Modifier.weight(1f))  // Spacer to push content to the top
        ElvButton(
            enabled = true,
            onClick = { /*TODO*/
                            navHostController.navigate("bookingDonePage")},
            btnTxt = "CONFIRM BOOKING NOW",
            Modifier
                .fillMaxWidth(1f)
                .requiredHeight(60.dp)
                .align(Alignment.CenterHorizontally)
        )
    }
}