package com.example.restaurantbookingsystem.screens

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.IconButtonColors
import androidx.compose.material3.IconButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.example.restaurantbookingsystem.GlobalVars
import com.example.restaurantbookingsystem.IndeterminateCircularIndicator
import com.example.restaurantbookingsystem.MySingleton
import com.example.restaurantbookingsystem.TitleText
import com.example.restaurantbookingsystem.ui.theme.clrBlack
import com.example.restaurantbookingsystem.ui.theme.clrBlackLight
import com.example.restaurantbookingsystem.ui.theme.clrLightGrey
import com.example.restaurantbookingsystem.ui.theme.clrOrange
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONException
import org.json.JSONObject
import java.nio.charset.Charset
import kotlin.coroutines.coroutineContext
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

@Composable
fun LocationSelect(context:Context, navHostController: NavHostController) {


    var searchText by remember { mutableStateOf(TextFieldValue()) }
    var itemList by remember { mutableStateOf(emptyList<String>()) }
    val page = rememberSaveable {   mutableIntStateOf(1) }
//    val page = remember {
//            CoroutineScope(Dispatchers.Default).launch {
//                mutableIntStateOf(getNumOfPage(context))
//            }
//         }
    val pageLimit = remember {   mutableIntStateOf(5) }
    LaunchedEffect(Unit) {
        itemList = getLocationList(context , page.intValue, pageLimit.intValue )
    }

    Log.i("Response log","$itemList ::LocationSelect")

//    var filteredList:List<String> = remember(searchText) {
//        itemList.filter { it.contains(searchText.text, ignoreCase = true) }
//    }
    var filteredList= remember(searchText) {
        mutableStateOf(   itemList.filter { it.contains(searchText.text, ignoreCase = true) }  )
    }

    var searchLocationList = mutableListOf<String>()

    Column(

        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top,
    ) {
        Spacer(modifier = Modifier.padding(vertical = 10.dp))
        TitleText(txt = "Select a location", fontsize = 40)

        OutlinedTextField(
            modifier = Modifier
                .padding(vertical = 10.dp, horizontal = 10.dp)
                .fillMaxWidth(1f),
            placeholder = { Text(text = "Enter your location") },
            value = searchText,
            onValueChange = {
                searchText = it

            },
            singleLine = true,
            label = { Text("Location") },
                keyboardOptions = KeyboardOptions(
                    imeAction = ImeAction.Search,
                    keyboardType = KeyboardType.Text
                ),
            visualTransformation = VisualTransformation.None,
            colors = OutlinedTextFieldDefaults.colors(
                focusedTextColor = clrBlack,
                unfocusedTextColor = clrBlack,
                focusedContainerColor = clrLightGrey,
                unfocusedContainerColor = clrLightGrey,
                disabledContainerColor = clrLightGrey,
                cursorColor = clrOrange,
                focusedBorderColor = Color.Black,
                unfocusedBorderColor = Color.Black,
                focusedLeadingIconColor = clrBlack,
                unfocusedLeadingIconColor = clrBlack,
                focusedLabelColor = clrBlack,
                unfocusedLabelColor = clrBlack,
                focusedPlaceholderColor = clrBlack,
                unfocusedPlaceholderColor = clrBlackLight,
            ),
            leadingIcon = {
                Icon(
                    imageVector = Icons.Filled.LocationOn,
                    contentDescription = null,
                    modifier = Modifier.padding(0.dp),
                )
            },
            trailingIcon = {
                Icon(
                    imageVector = Icons.Filled.Search,
                    contentDescription = "Search Icon",
                    tint = Color.Red,
                    modifier = Modifier
                        .padding(end = 5.dp)
                        .clickable {
                            CoroutineScope(Dispatchers.Main).launch {
                                Log.d("Response log", "Search button is clicked ::locationSelect")
                                searchLocationList = getSearchLocation(searchText.text, context)
                                filteredList.value = searchLocationList
                            }
                        }
                    ,
                )
            }
        )


        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .padding(top = 10.dp, bottom = 20.dp),
        ) {

            IconButton(
                enabled = page.intValue != 1,
                onClick = {
                    CoroutineScope(Dispatchers.Default).launch {
                        page.intValue -=1;
                        itemList = getLocationList(context , page.intValue, pageLimit.intValue )
                    }
                },
                colors = IconButtonDefaults.iconButtonColors(
                    containerColor = clrOrange,
                    contentColor = clrLightGrey
                ),
                ) {
                Icon(Icons.Filled.ArrowBack, contentDescription = "Arrow Icon")

            }
            TitleText(
                modifier = Modifier.padding(horizontal = 10.dp),
                txt = "Popular locations",
                fontsize = 30,
                txtStyle = MaterialTheme.typography.bodyMedium
            )
            IconButton(
                enabled = page.intValue != 3,
                onClick = {
                    page.intValue +=1;
                    CoroutineScope(Dispatchers.Default).launch {
                        itemList = getLocationList(context , page.intValue, pageLimit.intValue )
                    }
                },
                colors = IconButtonDefaults.iconButtonColors(
                containerColor = clrOrange,
                contentColor = clrLightGrey
            )) {
                Icon(Icons.Filled.ArrowForward, contentDescription = "Arrow Icon")
            }
        }


        if(itemList.isEmpty()) {
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                IndeterminateCircularIndicator(isLoading = true)
            }
        }
        else {
            LazyColumn(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.spacedBy(8.dp)
            ) {
                items(
                    if(filteredList.value.isEmpty() || searchText.text == "") itemList
                    else filteredList.value
                ) { item ->
                    Box(
                        modifier = Modifier
                            .padding(horizontal = 16.dp)
                            .fillMaxWidth()
                            .background(color = Color.White)
                            .shadow(elevation = 0.5.dp, shape = RoundedCornerShape(0.5.dp))
                            .clickable {
                                searchText = TextFieldValue("")
                                navHostController.navigate("restaurantsList/$item")
                            }

                            .padding(16.dp)
                    ) {
                        Row {
                            Icon(
                                modifier = Modifier.align(alignment = Alignment.CenterVertically),
                                imageVector = Icons.Filled.ArrowForward, contentDescription = "Location List icon" )
                            Text(
                                text = item,
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(16.dp)
                                    .clickable {
                                        searchText = TextFieldValue("")
                                        navHostController.navigate("restaurantsList/$item")
                                    },
                                fontWeight = FontWeight.W400,
                                fontSize = 20.sp
                            )
                        }
                    }
                }
            }
        }
    }
}


suspend fun getSearchLocation(searchString: String, context: Context): MutableList<String> {
    return withContext(Dispatchers.IO) {
        suspendCoroutine { continuation ->
            val url = "${GlobalVars.url}/api/locations"
            val token = GlobalVars.token
            val searchLocationList = mutableListOf<String>()
            try {
                val myStringRequest: StringRequest = object : StringRequest(
                    Method.POST,
                    url,
                    Response.Listener<String?> { response ->
                        val responseObj = JSONObject(response)
                        Log.d("Response log", "Json response is $responseObj")
                        if (responseObj.has("results")) {
                            val resultsJSONArray = responseObj.getJSONArray("results")

                            for(i in 0 until resultsJSONArray.length()) {
                                searchLocationList.add(resultsJSONArray.getJSONObject(i).getString("location"))
                            }
                            Log.d("Response log", "Out of the for loop $searchLocationList")
                            continuation.resume(searchLocationList)
                        }
                        else if(responseObj.has("message")) {
                            Log.e("Response log", " Search Location msg is $responseObj ::getSearchLocation")
                            Toast.makeText(context, responseObj.getString("message"), Toast.LENGTH_SHORT).show()
                        }
                        else {
                            Toast.makeText(context, "Error LMAO!!!" ,Toast.LENGTH_SHORT).show()
                        }
                    },
                    Response.ErrorListener { error ->
                        Log.e("Response log", "Error is $error ::getSearchLocation")
                        continuation.resumeWithException(error)
                    }
                ) {
                    override fun getHeaders(): MutableMap<String, String> {
                        val params: MutableMap<String, String> = HashMap()
                        params.put("Authorization", "bearer $token")
                        params.put("content-type", "application/Json; charset=UTF-8")
                        return params
                    }

                    override fun getBody(): ByteArray {
                        val params: LinkedHashMap<Any, Any> = LinkedHashMap()
                        params["search_location"] = searchString
                        val paramString = JSONObject(params as Map<*, *>?).toString()
                        return paramString.toByteArray(Charset.defaultCharset())
                    }
                }
                MySingleton.getInstance(context).addToRequestQueue(myStringRequest)
            } catch (e: JSONException) {
                Log.e("Response log", "Error is $e. Response log is in the catch block ::getSearchLocation")
                continuation.resumeWithException(e)
            }
        }
    }
}

suspend fun getNumOfPage(context: Context): Int {
    return suspendCoroutine { continuation ->
        var page: Int = 0
        val url = "${GlobalVars.url}/api/location"
        val myStringRequest: StringRequest = object : StringRequest(
            Method.POST,
            url,
            Response.Listener<String?> { response ->
                val jsonObject = JSONObject(response)
                if (jsonObject.has("totalPages")) {
                    page = jsonObject.getInt("totalPages")
                }
                continuation.resume(page)
            }, Response.ErrorListener { error ->
                continuation.resumeWithException(error)
            }
        ) {
            override fun getHeaders(): MutableMap<String, String> {
                val params: MutableMap<String, String> = HashMap()
                params["Authorization"] = "bearer $token"
                params["content-type"] = "application/Json; charset=UTF-8"
                return params
            }
        }
        MySingleton.getInstance(context).addToRequestQueue(myStringRequest)
    }
}


