package com.example.restaurantbookingsystem.screens

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.DatePicker
import androidx.compose.material3.DatePickerColors
import androidx.compose.material3.DatePickerDefaults
import androidx.compose.material3.DatePickerDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.rememberDatePickerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableIntState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableLongStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextMotion
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.DialogProperties
import androidx.navigation.NavHostController
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.chargemap.compose.numberpicker.NumberPicker
import com.example.restaurantbookingsystem.ElvButton
import com.example.restaurantbookingsystem.GlobalVars
import com.example.restaurantbookingsystem.MySingleton
import com.example.restaurantbookingsystem.R
import com.example.restaurantbookingsystem.TitleText
import com.example.restaurantbookingsystem.ui.theme.clrBlack
import com.example.restaurantbookingsystem.ui.theme.clrBlackLight
import com.example.restaurantbookingsystem.ui.theme.clrLightGreyTrans
import com.example.restaurantbookingsystem.ui.theme.clrLightGreyTrans2
import com.example.restaurantbookingsystem.ui.theme.clrOrange
import com.google.android.material.datepicker.MaterialDatePicker
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONException
import org.json.JSONObject
import java.nio.charset.Charset
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.Calendar
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

fun formatDateFromTimestamp(timestamp: Long): String {
    val dateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), ZoneOffset.UTC)
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    return dateTime.format(formatter)
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RestaurantBook(navController: NavHostController, context: Context, restaurantId: Int, restaurantName: String, imageURL: String) {
    Log.d("Response log" , "image url is $imageURL ::RestaurantBook")
    val pickerValue: MutableIntState = remember { mutableIntStateOf(2) }

    val calendar = Calendar.getInstance()
    calendar.timeInMillis = System.currentTimeMillis() // add year, month (Jan), date

    // set the initial date
    val datePickerState = rememberDatePickerState(
        initialSelectedDateMillis = calendar.timeInMillis,)
    // minDate = calendar.timeInMillis

    var showDatePicker by remember {
        mutableStateOf(false)
    }

    var selectedDate by remember {
mutableLongStateOf(calendar.timeInMillis) // or use mutableStateOf(calendar.timeInMillis)
    }
    val formattedDate = rememberSaveable {  mutableStateOf(formatDateFromTimestamp(selectedDate))   }
//        remember {mutableStateOf(formatDateFromTimestamp(selectedDate))}

    GlobalVars.restId = restaurantId
    GlobalVars.restName = restaurantName
    GlobalVars.bookingDate = formattedDate.value
    GlobalVars.noOfGuests = pickerValue.intValue

    Column() {

        ImageAndTextBox(imageURL, restaurantName)

        Spacer(modifier = Modifier.padding(vertical = 20.dp))

        RestaurantDescriptionCard()

        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .padding(horizontal = 20.dp)
                .fillMaxWidth(1f)
                .background(color = Color.White)
                .shadow(elevation = 0.5.dp, shape = RoundedCornerShape(0.5.dp))


        ) {
            ElvButton(
                onClick = {
                    showDatePicker = true
                },
                btnTxt = "Pick a date",
                modifier = Modifier.padding(10.dp)
            )
            Text(text = formattedDate.value, color = clrBlack, fontWeight = FontWeight.Medium, modifier = Modifier.padding(horizontal = 15.dp))
        }
        Spacer(modifier = Modifier.padding(vertical = 10.dp))
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .padding(horizontal = 20.dp)
                .fillMaxWidth(1f)
                .background(color = Color.White)
                .shadow(elevation = 0.5.dp, shape = RoundedCornerShape(0.5.dp))

        ) {
            ElvButton(
                onClick = { /*TODO*/ },
                btnTxt = "No. of Guests" ,
                modifier = Modifier.padding(10.dp)
            )

            Box(modifier = Modifier.padding(horizontal = 15.dp)) {
                NumberPicker(
                    value = pickerValue.intValue,
                    range = 2..10,
                    onValueChange = {
                        pickerValue.intValue = it
                    },
                    dividersColor = clrOrange,
                    textStyle = TextStyle(textMotion = TextMotion.Animated, fontWeight = FontWeight.Medium)
                )
            }
        }

            if (showDatePicker) {
                DatePickerDialog(
                    properties = DialogProperties(dismissOnBackPress = false),
                    colors = DatePickerDefaults.colors(containerColor = clrBlackLight),
                    onDismissRequest = {
                        showDatePicker = false
                    },
                    confirmButton = {
                        TextButton(onClick = {
                            showDatePicker = false
                            selectedDate = datePickerState.selectedDateMillis!!
                            formattedDate.value = formatDateFromTimestamp(selectedDate)
                            Log.i("Response log", "$formattedDate pick a date dialog confirm btn ::restaurantBook")
                        }) {
                            Text(text = "Confirm")
                        }
                    },
                    dismissButton = {
                        TextButton(onClick = {
                            showDatePicker = false
                        }) {
                            Text(text = "Cancel")
                        }
                    }
                ) {
                    DatePicker(
                        state = datePickerState
                    )
                }

}

        Spacer(modifier = Modifier.weight(1f))  // Spacer to push content to the top
        ElvButton(
            enabled = true,
            onClick = {
                Log.i("Response log", "parameter received is $restaurantId, $formattedDate,${pickerValue.intValue} ::RestaurantBook")

                CoroutineScope(Dispatchers.Main).launch {
                    val slotList = withContext(Dispatchers.IO) {
                        getSlots(restaurantId, formattedDate.value, pickerValue.intValue, context)
                    }
                    var slotString = slotList.toString()
                    slotString = slotString.substring(1, slotString.length - 1)
                    Log.d("Response log", "slot String is $slotString ::RestaurantBook")
                    navController.navigate("SlotsDetail/$slotString")
                }
            },

            btnTxt = "GET BOOKING SLOTS",
            Modifier
                .height(60.dp)
                .fillMaxWidth(1f)

//            .align(Alignment.CenterHorizontally)

        )
    }


}

@Composable
private fun ImageAndTextBox(imageURL: String, restaurantName: String) {
    Box(modifier = Modifier.height(200.dp)) {

        AsyncImage(
            contentScale = ContentScale.Crop,
            model = ImageRequest.Builder(LocalContext.current)
                .data(imageURL)
                .crossfade(true)
                .build(),
            contentDescription = "Restaurant Image",

            modifier = Modifier
                .fillMaxWidth()
                .aspectRatio(1f)
                .clip(RoundedCornerShape(16.dp)) // Adjust the corner radius as needed
                .shadow(
                    elevation = 0.dp,
                    shape = RoundedCornerShape(16.dp)
                ) // Add shadow to the rounded corners
        )
        TitleText(
            txt = "\n $restaurantName",
            fontsize = 45,
            txtStyle = TextStyle(
                fontWeight = FontWeight.Bold,
                letterSpacing = 1.5.sp
            ), //MaterialTheme.typography.titleLarge
            clr = clrLightGreyTrans2,
            txtAlign = TextAlign.Left,
            Modifier
                .fillMaxWidth(1f)
                .padding(horizontal = 0.dp, vertical = 0.dp)
                .background(color = clrLightGreyTrans)
                .align(Alignment.BottomStart)
        )

    }
}

suspend fun getSlots(restaurantId: Int, formattedDate: String, nguests: Int = 2, context: Context): List<String> {
    return suspendCoroutine { continuation ->
        val url = "${GlobalVars.url}/api/availableSlot"
        val token = GlobalVars.token

        try {
            val myStringRequest: StringRequest = object : StringRequest(
                Method.POST,
                url,
                Response.Listener<String?> { response ->
                    val jsonObject = JSONObject(response)
                    if (jsonObject.has("remainingSlots")) {
                        val slotsArray = jsonObject.getJSONArray("remainingSlots")
                        val slotsList = mutableListOf<String>()
                        for (i in 0 until slotsArray.length()) {
                            val slot = slotsArray.getString(i)
                            slotsList.add(slot)
                        }
                        continuation.resume(slotsList)
                    }
                    else if(jsonObject.has("message")) {
                        Log.e("Response log", " $jsonObject Pick a current or future date. Your date is  $formattedDate")
                        Toast.makeText(context, jsonObject.getString("message"), Toast.LENGTH_SHORT).show()
                    }
                },
                Response.ErrorListener { error ->
                    Log.e("Response log", "Error is $error ::getSlots")
                    continuation.resumeWithException(error)
                }
            ) {
                override fun getHeaders(): MutableMap<String, String> {
                    val params: MutableMap<String, String> = HashMap()
                    params.put("Authorization", "bearer $token")
                    params.put("content-type", "application/Json; charset=UTF-8")
                    return params
                }

                override fun getBody(): ByteArray {
                    val params: LinkedHashMap<Any, Any> = LinkedHashMap()
                    params["restaurant_id"] = restaurantId
                    params["selected_date"] = formattedDate
                    params["nguests"] = nguests
                    val paramString = JSONObject(params as Map<*, *>?).toString()
                    return paramString.toByteArray(Charset.defaultCharset())
                }
            }
            MySingleton.getInstance(context).addToRequestQueue(myStringRequest)
        } catch (e: JSONException) {
            Log.e("Response log", "Error is $e. Response log is in the catch block::getSlots")
            continuation.resumeWithException(e)
        }
    }
}

@Composable
fun RestaurantDescriptionCard() {
    Box(
        modifier = Modifier
            .padding(horizontal = 20.dp, vertical = 20.dp)
            .fillMaxWidth(1f)
            .background(color = Color.White)
            .shadow(
                elevation = 0.5.dp,
                shape = RoundedCornerShape(0.5.dp)
            ),
        ) {
        Column(Modifier
            .padding(horizontal = 15.dp, vertical = 15.dp)) {
            Text(
                modifier = Modifier.fillMaxWidth(1f),
                text = "Restaurant Overview",
                softWrap = true,
                color = clrBlack,
                fontWeight = FontWeight.Medium,
                lineHeight = 15.sp,
            )
            Text(
                modifier = Modifier.padding(),
                softWrap = true,
                textAlign = TextAlign.Justify,
                color = clrBlackLight,
                fontWeight = FontWeight.Light,
                lineHeight = 15.sp,
                text = stringResource(id = R.string.loremIpsum),
            )
        }
    }
}

