package com.example.restaurantbookingsystem.screens

import android.content.Context
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material.icons.rounded.Face
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.navigation.NavHostController
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.example.restaurantbookingsystem.ElvButton
import com.example.restaurantbookingsystem.GlobalVars
import com.example.restaurantbookingsystem.MySingleton
import com.example.restaurantbookingsystem.TxtBtn
import com.example.restaurantbookingsystem.ui.theme.clrBlack
import com.example.restaurantbookingsystem.ui.theme.clrBlackLight
import com.example.restaurantbookingsystem.ui.theme.clrLightGrey
import com.example.restaurantbookingsystem.ui.theme.clrOrange
import org.json.JSONException
import org.json.JSONObject
import java.nio.charset.Charset
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

var token = ""

@Composable
    fun LoginPage(navController: NavHostController, context: Context) {


    val url = "${GlobalVars.url}/api/login"
        //val url = "http://10.0.2.2:3001/api/login"

        val btnEnabled = remember { mutableStateOf(false)  }
        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Top,
        ) {
//            val isLoggedIn by getLoginStatus(context).collectAsState(initial = false)
//            val coroutineScope = rememberCoroutineScope()
            Text(
                text = "Restaurant Booking System",
                style = MaterialTheme.typography.headlineLarge,
                color = clrOrange,
                modifier = Modifier
                    .padding(vertical = 50.dp)
                    .clickable {
//                        coroutineScope.launch {
//                            changeLoginStatus(context, true)
//                        }
                    },
            )
            Spacer(modifier = Modifier.padding(vertical = 70.dp))

            val enteredEmail = remember {   mutableStateOf("")  }
            val isErrorInEmail = remember {
                derivedStateOf {
                    if(enteredEmail.value.isEmpty()) {
                        false
                    }
                    else {
                        Patterns.EMAIL_ADDRESS.matcher(enteredEmail.value).matches().not()

                    }
                }
            }
            EmailField(enteredEmail, isErrorInEmail)



            val enteredPassword = remember {
                mutableStateOf("")
            }
            val isErrorInPassword = remember {
                derivedStateOf {
                    if(enteredPassword.value.isEmpty()) {
                        false
                    }
                    else {
                        enteredPassword.value.length < 8
                    }
                }
            }
            val isPasswordVisible = remember { mutableStateOf(false) }

            PasswordField(
                enteredPassword,
                btnEnabled,
                isErrorInPassword,
                isErrorInEmail,
                isPasswordVisible
            )

            Spacer(modifier = Modifier.padding(vertical = 15.dp))
                ElvButton(
                    enabled = btnEnabled.value,
                    onClick =
                    loginOnClickFun(
                        isErrorInEmail,
                        isErrorInPassword,
                        context,
                        btnEnabled,
                        url,
                        navController,
                        enteredEmail,
                        enteredPassword
                    ),
                    btnTxt = "Login",
                    modifier = Modifier
                        .width(170.dp)
                        .height(70.dp)
                )

            Spacer(modifier = Modifier.padding(vertical = 10.dp))

            TxtBtn(
                onClick = {
                    navController.navigate("SignUpPage")
                },
                txt = "Don't have a account?\n Sign Up"
            )
        }
    }

@Composable
private fun loginOnClickFun(
    isErrorInEmail: State<Boolean>,
    isErrorInPassword: State<Boolean>,
    context: Context,
    btnEnabled: MutableState<Boolean>,
    url: String,
    navController: NavHostController,
    enteredEmail: MutableState<String>,
    enteredPassword: MutableState<String>
): () -> Unit = {
    if (isErrorInEmail.value || isErrorInPassword.value) {
        Toast.makeText(context, "Wrong username or password format", Toast.LENGTH_SHORT).show()
    } else {
        btnEnabled.value = true
        val myStringRequest: StringRequest = object : StringRequest(Method.POST, url,
            Response.Listener<String?> { response ->
                if (response != null) {
                    val jsonObject = JSONObject(response)

                    if (jsonObject.has("token")) {

                        token = jsonObject.getString("token")

                        GlobalVars.token = token
                        GlobalVars.isLoggedIn = true
                        navController.popBackStack()
                        navController.navigate("LocationSelect")


                    } else {
                        Toast.makeText(
                            context,
                            "Wrong Credentials or Email",
                            Toast.LENGTH_SHORT
                        ).show()
                        val message = jsonObject.getString("message")

                    }
                } else {
                    Toast.makeText(context, "Response is NULL", Toast.LENGTH_SHORT).show()
                }

                try {


                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }, Response.ErrorListener { // displaying toast message on response failure.
                Toast.makeText(context, "Fail to post data..", Toast.LENGTH_SHORT)
                    .show()
            }) {

            override fun getHeaders(): MutableMap<String, String> {
                val params: MutableMap<String, String> = HashMap()
                params.put("content-type", "application/Json; charset=UTF-8")
                return params
            }

            override fun getBody(): ByteArray {
                val params: LinkedHashMap<Any, Any> = LinkedHashMap()

                params["email"] = enteredEmail.value
                params["password"] = enteredPassword.value

                val paramString = JSONObject(params as Map<*, *>?).toString()
                return paramString.toByteArray(Charset.defaultCharset())
            }

        }
        MySingleton.getInstance(context).addToRequestQueue(myStringRequest)
    }
}

@Composable
private fun PasswordField(
    enteredPassword: MutableState<String>,
    btnEnabled: MutableState<Boolean>,
    isErrorInPassword: State<Boolean>,
    isErrorInEmail: State<Boolean>,
    isPasswordVisible: MutableState<Boolean>
) {
    OutlinedTextField(
        value = enteredPassword.value,
        onValueChange = { it: String ->
            enteredPassword.value = it
            btnEnabled.value = !isErrorInPassword.value && !isErrorInEmail.value
        },
        singleLine = true,
        placeholder = { Text("Enter your Password") },
        label = { Text("Password") },
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Password,
            imeAction = ImeAction.Done
        ),
        visualTransformation = if (isPasswordVisible.value) VisualTransformation.None else PasswordVisualTransformation(),


        leadingIcon = { Icon(Icons.Default.Lock, contentDescription = "Lock Icon") },
        trailingIcon = {
            Icon(
                imageVector = Icons.Rounded.Face,
                tint = clrBlack,
                contentDescription = "Password Visible Icon",
                modifier = Modifier.clickable {
                    isPasswordVisible.value = !isPasswordVisible.value
                }
            )
        },
        supportingText = {
            if (isErrorInPassword.value) {
                Text(text = "Password length must be greater than 8 ", fontSize = 14.sp)
            }
        },
        isError = isErrorInPassword.value,
        colors = OutlinedTextFieldDefaults.colors(
            focusedTextColor = clrBlack,
            unfocusedTextColor = clrBlack,
            focusedContainerColor = clrLightGrey,
            unfocusedContainerColor = clrLightGrey,
            disabledContainerColor = clrLightGrey,
            disabledTextColor = clrBlack,
            cursorColor = clrOrange,
            focusedBorderColor = clrOrange,
            unfocusedBorderColor = clrBlackLight,
            focusedLeadingIconColor = clrBlack,
            unfocusedLeadingIconColor = clrBlack,
            focusedLabelColor = clrBlack,
            unfocusedLabelColor = clrBlack,
            focusedPlaceholderColor = clrBlack,
            unfocusedPlaceholderColor = clrBlackLight,

            ),
    )
}

@Composable
private fun EmailField(
    enteredEmail: MutableState<String>,
    isErrorInEmail: State<Boolean>
) {
    OutlinedTextField(
        value = enteredEmail.value,
        onValueChange = { it: String ->
            enteredEmail.value = it
        },
        placeholder = { Text("Enter your Email") },
        label = { Text("Email") },
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Email,
            imeAction = ImeAction.Next
        ),
        singleLine = true,
        leadingIcon = { Icon(Icons.Default.Email, contentDescription = "Email Icon") },
        supportingText = {
            if (isErrorInEmail.value) {
                Text(text = "Enter a valid email address")
            }
        },
        isError = isErrorInEmail.value,
        colors = OutlinedTextFieldDefaults.colors(
            focusedTextColor = clrBlack,
            unfocusedTextColor = clrBlack,
            focusedContainerColor = clrLightGrey,
            unfocusedContainerColor = clrLightGrey,
            disabledContainerColor = clrLightGrey,
            cursorColor = clrOrange,
            focusedBorderColor = clrOrange,
            unfocusedBorderColor = clrBlackLight,
            focusedLeadingIconColor = clrBlack,
            unfocusedLeadingIconColor = clrBlack,
            focusedLabelColor = clrBlack,
            unfocusedLabelColor = clrBlack,
            focusedPlaceholderColor = clrBlack,
            unfocusedPlaceholderColor = clrBlackLight,
        ),
    )
}

suspend fun getLocationList(context: Context, page: Int, limit:Int ): MutableList<String> {

    val url = "${GlobalVars.url}/api/location" //location?page=$page&limit=$limit
    val myList = mutableListOf<String>()
    try {
        suspendCoroutine<Unit> { continuation ->
            val myStringRequest: StringRequest = object : StringRequest(
                Method.POST,
                url,
                Response.Listener<String?> { response ->
                    val jsonObject = JSONObject(response)

                    if (jsonObject.has("results")) {
                        val locationsArray = jsonObject.getJSONArray("results")

                        for (i in 0 until locationsArray.length()) {
                            val resultObject = locationsArray.getJSONObject(i)
                            val location = resultObject.getString("location")
                            myList.add(location)
                            Log.d("Response log", "In the for loop $location ::getLocationList")
                        }
                        Log.i("Response log", "End of for loop::getLocationList")
                    }
                    continuation.resume(Unit)
                }, Response.ErrorListener { error ->
                    Log.e("Response log", "Error response is $error::getLocationList")
                    Toast.makeText(context, "Error! Failed to post data.", Toast.LENGTH_SHORT).show()
                    continuation.resumeWithException(error)
                }) {

                override fun getHeaders(): MutableMap<String, String> {
                    val params: MutableMap<String, String> = HashMap()
                    params["Authorization"] = "bearer $token"
                    params["content-type"] = "application/Json; charset=UTF-8"
                    return params
                }

                override fun getBody(): ByteArray {
                    val params: LinkedHashMap<Any, Any> = LinkedHashMap()
                    params["limit"] = limit.toString()
                    params["page"] = page.toString()

                val paramString = JSONObject(params as Map<*, *>?).toString()
                Log.d("TAG", paramString)
                return paramString.toByteArray(Charset.defaultCharset())
            }



            }
            MySingleton.getInstance(context).addToRequestQueue(myStringRequest)
        }
    } catch (e: JSONException) {
        Log.e("Response log", "Response log is in the catch block::getLocationList")
        e.printStackTrace()
    }

    return myList
}


