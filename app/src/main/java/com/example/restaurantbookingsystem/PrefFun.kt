package com.example.restaurantbookingsystem

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.first

private val isLoggedInKey = booleanPreferencesKey("is_logged_in")
private val tokenKey = stringPreferencesKey("tokenKey")

val Context.dstore: DataStore<Preferences> by preferencesDataStore("my_preferences")

suspend fun readLoginState(datastore: DataStore<Preferences>): LoginState {
    val preferences = datastore.data.first()
    val isLoggedIn = preferences[isLoggedInKey] ?: false
    val token = preferences[tokenKey]?.let { LoginToken(it) }
    return LoginState(isLoggedIn, token)
}

suspend fun saveLoginState(datastore: DataStore<Preferences>, isLoggedIn: Boolean, token: String?) {
    datastore.updateData { currentPreferences ->
        val updatedPreferences = currentPreferences.toMutablePreferences()
        updatedPreferences[isLoggedInKey] = isLoggedIn
        token?.let { updatedPreferences[tokenKey] = it }
        updatedPreferences
    }
}

