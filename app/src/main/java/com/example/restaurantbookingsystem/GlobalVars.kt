package com.example.restaurantbookingsystem

import androidx.compose.runtime.remember

object GlobalVars {
    var token:String = ""
    var isLoggedIn:Boolean = false

    var restId:Int = 0
    var restName = ""
    var bookingDate = ""
    var bookingTime = ""
    var noOfGuests:Int = 0
    var url = "http://192.168.0.107:3001"

    //Emulator IP "http://10.0.2.2:3001/"
    //PG IP "http://192.168.153.110:3001"
    //Office IP "http://192.168.3.81:3001"
    //Aman Pc IP "http://192.168.3.149:3001"
    //

}