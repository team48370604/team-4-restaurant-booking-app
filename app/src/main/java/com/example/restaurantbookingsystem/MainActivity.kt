package com.example.restaurantbookingsystem

//import com.example.restaurantbookingsystem.screens.LoginPage
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.datastore.core.DataStore
import androidx.datastore.dataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.restaurantbookingsystem.screens.BookingDetails
import com.example.restaurantbookingsystem.screens.BookingDonePage
import com.example.restaurantbookingsystem.screens.LocationSelect
import com.example.restaurantbookingsystem.screens.LoginPage
import com.example.restaurantbookingsystem.screens.RestaurantBook
import com.example.restaurantbookingsystem.screens.RestaurantsList
import com.example.restaurantbookingsystem.screens.SignUpPage
import com.example.restaurantbookingsystem.screens.SlotsDetail
import com.example.restaurantbookingsystem.ui.theme.RestaurantBookingSystemTheme
import com.example.restaurantbookingsystem.ui.theme.clrLightGrey
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map


class MainActivity : ComponentActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            RestaurantBookingSystemTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier
                        .fillMaxSize(),
                    color = clrLightGrey

                ) {

                    val navController = rememberNavController()
                    val strtDest:String =  "LoginPage"

                    NavHost(
                        navController = navController,
                        startDestination = strtDest
                    ) {
                    composable(route = "LoginPage" ) {
                        LoginPage( navController, context = LocalContext.current)
                    }

                        composable("SignUpPage") {
                            SignUpPage(context = LocalContext.current, navController)
                        }
                        composable("LocationSelect") {
                            LocationSelect(context = LocalContext.current, navHostController = navController )
                        }
                        composable("restaurantsList/{selectedCity}") {
                            /* TODO */
                            navBackStackEntry -> val selectedCity = navBackStackEntry.arguments?.getString("selectedCity")
                            selectedCity?.let {
                                RestaurantsList(navController, context = LocalContext.current, selectedCity = selectedCity)
                            }

                        }


                        composable(
                            route = "restaurantBook/{restaurantId}/{restaurantName}/{imageURL}",
                            arguments = listOf(
                                navArgument("restaurantId") { type = NavType.IntType },
                                navArgument("restaurantName") { type = NavType.StringType },
                                navArgument("imageURL") { type = NavType.StringType }
                            )
                        ) { navBackStackEntry ->
                            // Access arguments directly without the need for nested checks
                            val restaurantId = navBackStackEntry.arguments?.getInt("restaurantId") ?: error("No restaurantId provided")
                            val restaurantName = navBackStackEntry.arguments?.getString("restaurantName") ?: error("No restaurantName provided")
                            val imageURL = navBackStackEntry.arguments?.getString("imageURL") ?: error("No imageURL provided")

                            RestaurantBook(
                                navController,
                                context = LocalContext.current,
                                restaurantId = restaurantId,
                                restaurantName = restaurantName,
                                imageURL = imageURL
                            )
                        }





                        composable(
                            route = "bookingDetails/{restaurantName}/{restaurantAddress}/{bookingDate}/{formattedTime}/{nguest}/{phoneNum}",
                            arguments = listOf(
                                navArgument("restaurantName") {
                                    type = NavType.StringType
                                },
                                navArgument("restaurantAddress") {
                                    type = NavType.StringType
                                },
                                navArgument("bookingDate") {
                                    type = NavType.StringType
                                },
                                navArgument("formattedTime") {
                                    type = NavType.StringType
                                },
                                navArgument("nguest") {
                                    type = NavType.StringType
                                },
                                navArgument("phoneNum") {
                                    type = NavType.StringType
                                }
                            )
                        ) { navBackStackEntry ->
                            val restaurantName = navBackStackEntry.arguments?.getString("restaurantName")
                            val restaurantAddress = navBackStackEntry.arguments?.getString("restaurantAddress")
                            val bookingDate = navBackStackEntry.arguments?.getString("bookingDate")
                            val formattedTime = navBackStackEntry.arguments?.getString("formattedTime")
                            val nguest = navBackStackEntry.arguments?.getString("nguest")
                            val phoneNum = navBackStackEntry.arguments?.getString("phoneNum")

                            restaurantName?.let { name ->
                                restaurantAddress?.let { address ->
                                    bookingDate?.let { date ->
                                        formattedTime?.let { time ->
                                            nguest?.let { guests ->
                                                phoneNum?.let { phone ->
                                                    BookingDetails(
                                                        navController,
                                                        name,
                                                        address,
                                                        date,
                                                        time,
                                                        guests,
                                                        phone
                                                    )
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }


                        composable(
                            route ="bookingDonePage/{bookingID}",
                            arguments = listOf(
                                navArgument("bookingID") {
                                    type = NavType.IntType
                                }
                            )
                        ){
                            it.arguments?.let { it2 ->
                                BookingDonePage(navController,
                                    LocalContext.current, GlobalVars.restName, GlobalVars.bookingDate, GlobalVars.noOfGuests ,
                                    it2.getInt("bookingID")
                                )
                            }
                        }

                        composable(
                            route = "SlotsDetail/{slotList}",
                            arguments = listOf(
                                navArgument("slotList") {
                                    type = NavType.StringType
                                }
                            )
                        ) {
                            it.arguments?.let { it2 -> it2.getString("slotList")
                                ?.let { it2 -> SlotsDetail( navController,context = LocalContext.current, slotList = it2)} }
                        }
                    }
                }
            }


        }
    }

}
