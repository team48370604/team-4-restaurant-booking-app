package com.example.restaurantbookingsystem

import androidx.datastore.preferences.core.stringPreferencesKey

object PreferenceKeys {
    val  token = stringPreferencesKey("TOKEN")
}