package com.example.restaurantbookingsystem

data class LoginState(val isLoggedIn: Boolean, val token: LoginToken?)
data class LoginToken(val value: String)